import os
import tempfile
import logging
import subprocess
from itertools import combinations
from collections import OrderedDict

import click
from path import Path
import numpy as np

from sblu import PRMS_DIR
from sblu.pdb import parse_pdb_stream, fix_atom_records
from sblu.util import which

from . import _splitsegs

DISU_THRESH_MIN = 1.28925530695 ** 2
DISU_THRESH_MAX = 2.82114477374 ** 2

logger = logging.getLogger(__name__)


def _patches_str_to_dict(s):
    """
    Convert --patch-first and --patch-last arguments to dictionary.
    :param s: comma-separated list of chains and corresponding patches, e.g., `A,CTER,D,CTER`.
    :returns: dictionary chain -> patch, e.g., `{'A': 'CTER', 'D': 'CTER'}`.
    """
    if not s:
        return dict()
    tokens = s.split(',')
    if len(tokens) % 2 != 0:
        raise ValueError('--patch-... parameter should contain even number of comma-separated tokens')
    return {
        chain: patch for patch, chain in zip(tokens[::2], tokens[1::2])
    }


def _links_str_to_list(s):
    """
    Convert --link arguments to list of dictionaries.
    :param s: comma-separated list of residue specifiers and corresponding patches, e.g.,
    `DISU,A,13,B,42,DISU,A,10,A,20`.
    :returns: list of dictionaries, each containing linker type, chain IDs and residue IDs, e.g.,
    `[{'type': 'DISU', 'chain1': 'A', 'chain2': 'B', 'res1': 13, 'res2': 42},
      {'type': 'DISU', 'chain1': 'A', 'chain2': 'A', 'res1': 10, 'res2': 20}]`.
    """
    if not s:
        return []
    tokens = s.split(',')
    if len(tokens) % 5 != 0:
        raise ValueError('Link parameter should contain number tokens that is multiple of 5')
    return [
        dict(type=type_, chain1=ch1, res1=int(res1), chain2=ch2, res2=int(res2))
        for type_, ch1, res1, ch2, res2 in zip(tokens[::5], tokens[1::5], tokens[2::5], tokens[3::5], tokens[4::5])
    ]


@click.command('prep', short_help="Prepare a PDB file for docking with PIPER.")
@click.option("--clean/--no-clean", "clean_pdb", default=True, help="Perform some common sanitizing of input PDB file. Default: True")
@click.option("--minimize/--no-minimize", default=True, help="Run short minimization on the resulting structure. Default: True")
@click.option("--xplor-psf/--no-xplor-psf", default=False, help="Output PSF in XPLOR format, in addition to CHARMM. Default: False")
@click.option("--smod", default="", help="Custom segment prefix.")
@click.option("--out-prefix", help="Prefix for output PDB/PSF files. Default: 'prepared'")
@click.option("--prm", type=click.Path(exists=True), default=PRMS_DIR/"charmm"/"charmm_param.prm")
@click.option("--rtf", type=click.Path(exists=True), default=PRMS_DIR/"charmm"/"charmm_param.rtf")
@click.option("chains", "--chain", multiple=True, help="Process only specified chains from input PDB. Default: process all chains")
@click.option("--psfgen", default="psfgen", help="Path to psfgen binary from NAMD")
@click.option("--nmin", default="nmin", help="Path to nmin binary from BU toolset")
@click.option("--nsteps", default=1000, type=click.INT, help="Number of steps for minimization. Default: 1000")
@click.option("--auto-disu/--no-auto-disu", default=True, help="Automatically add disulfide bonds. Default: True")
@click.option("--patch-first", default="", help="Custom patches for N-termini of chains. E.g., 'A,NTER,B,NTER' will apply standard protein N-terminal patches to chains A and B. Default: NONE")
@click.option("--patch-last", default="", help="Custom patches for C-termini of chains. E.g., 'A,CTER,B,CTER' will apply standard protein C-terminal patches to chains A and B. Default: NONE")
@click.option("--link", default="", help="Custom linker patches. E.g., 'DISU,A,4,A,13' will link residues A:4 and A:13 with disulfide bond. This applies in addition to --auto-disu patches, if any. Default: nothing")
@click.option("--delete-tmp/--no-delete-tmp", default=True, help="Delete temporary files. Default: True")
@click.argument("pdb_file", type=click.File(mode='r'))
def cli(pdb_file, chains, smod, clean_pdb, minimize, prm, rtf, xplor_psf,
        out_prefix, psfgen, nmin, nsteps, auto_disu, patch_first, patch_last, link, delete_tmp):
    def is_cys_sulfur(r):
        return r.name == ' SG ' and r.resname == 'CYS'

    workdir = Path(tempfile.mkdtemp())
    if not workdir:
        workdir = Path(".")
        delete_tmp = False
    else:
        logger.info('created tempdir {}'.format(workdir))

    if not out_prefix:
        out_prefix = "prepared"

    out_prefix = os.path.join(os.path.abspath("."), out_prefix)

    try:
        psfgen_bin = which(psfgen, required=True)

        if clean_pdb:
            records = list(fix_atom_records(parse_pdb_stream(pdb_file,
                                                             only_atoms=True)))
        else:
            records = list(parse_pdb_stream(pdb_file, only_atoms=True))

        all_segments = _splitsegs(records, smod, workdir / "segment")
        is_wildcard = len(chains) == 0 or '?' in chains
        segments = OrderedDict(
            item
            for item in all_segments.items()
            if (item[0][1] in chains or  # chain is explicitly named
                (is_wildcard and item[0][1][0] != 'h'))  # wildcard and not het
        )
        records = list(r
                       for segment in segments.values()
                       for r in parse_pdb_stream(open(segment)))

        p = subprocess.Popen([psfgen_bin],
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        commands = []
        commands.append("psfcontext mixedcase")
        commands.append("topology {0}".format(rtf))
        coord_commands = []

        # Parse explicit patches specified by `--patch-first` and `--patch-last` and check them for correctness
        patches_first_dict = _patches_str_to_dict(patch_first)
        patches_last_dict = _patches_str_to_dict(patch_last)

        chains = set(chain for segid, chain in segments.keys())
        if not chains.issuperset(patches_first_dict.keys()):
            logger.warning('--patch-first contains patches for non-existent chains')
        if not chains.issuperset(patches_last_dict.keys()):
            logger.warning('--patch-last contains patches for non-existent chains')

        # Load coordinates of all chains and patch first/last residues
        for (segid, chain), segment in segments.items():
            segid = smod + chain + str(segid)
            # Define segment and patches
            seg_cmd = "segment {segid} {{ first {pf}; last {pl}; pdb {segment} }}".format(
                segid=segid, segment=segment,
                pf=patches_first_dict.get(chain, 'NONE'),
                pl=patches_last_dict.get(chain, 'NONE'))
            commands.append(seg_cmd)
            # Define coordinates
            coord_cmd = "coordpdb {0} {1}".format(segment, segid)
            coord_commands.append(coord_cmd)

        # Parse custom link patches specified by `--link`
        custom_patches = _links_str_to_list(link)
        # Same chain get be split into multiple segments, so we have to parse PDB to find out which segment
        # corresponds to the (chain, resid) given as `--link` arguments.
        residue_to_segment = dict()
        for patch in custom_patches:  # Add them to the dictionary as None, so we know we should look for them.
            residue_to_segment[(patch['chain1'], patch['res1'])] = None
            residue_to_segment[(patch['chain2'], patch['res2'])] = None
        for segment in segments.values():
            segment = list(parse_pdb_stream(open(segment, "r")))
            for record in segment:
                k = (record.chain, record.resnum)
                if k in residue_to_segment.keys():
                    residue_to_segment[k] = record.segment.strip()
        # Check that we found all residues that should be linked
        for residue, segid in residue_to_segment.items():
            if segid is None:
                logging.warning('Asked to link residue {}:{}, but unable to find it'.format(*residue))
        # Add patches to link residues
        for patch in custom_patches:
            seg1 = residue_to_segment[(patch['chain1'], patch['res1'])]
            seg2 = residue_to_segment[(patch['chain2'], patch['res2'])]
            if seg1 is not None and seg2 is not None:
                commands.append(
                    "patch {type} {seg1}:{res1} {seg2}:{res2}".format(seg1=seg1, seg2=seg2, **patch))

        # Patch noncontinuous chains
        previous_chain = None
        previous_carbon = None
        for (segid, chain), segment in segments.items():
            if previous_chain is not None and previous_chain != chain:
                previous_carbon = None
                previous_chain = chain
                continue

            segment = list(parse_pdb_stream(open(segment, "r")))

            if previous_carbon:
                nitrogen = None
                for record in segment:
                    if record.name == " N  ":
                        nitrogen = record
                        break

                if nitrogen is not None:
                    delta = nitrogen.coords - previous_carbon.coords
                    dist = np.sqrt(np.sum(np.multiply(delta, delta, delta)))
                    if dist < 1.4:
                        link = (previous_carbon.segment.strip(),
                                previous_carbon.resnum,
                                nitrogen.segment.strip(), nitrogen.resnum)

                        if previous_carbon.resname == "GLY":
                            if nitrogen.resname == "GLY":
                                commands.append(
                                    "patch JOGG {}:{} {}:{}".format(*link))
                            elif nitrogen.resname == "PRO":
                                commands.append(
                                    "patch JOGP {}:{} {}:{}".format(*link))
                            else:
                                commands.append(
                                    "patch JOGA {}:{} {}:{}".format(*link))
                        elif nitrogen.resname == "PRO":
                            commands.append("patch JOAP {}:{} {}:{}".format(
                                *link))
                        elif nitrogen.resname == "GLY":
                            commands.append("patch JOAG {}:{} {}:{}".format(
                                *link))
                        else:
                            commands.append("patch JOAA {}:{} {}:{}".format(
                                *link))

            previous_carbon = None
            for record in segment[::-1]:
                if record.name == " C  ":
                    previous_carbon = record
                    break
            previous_chain = chain

        disulfides = []
        if auto_disu:
            for ri, rj in combinations(filter(is_cys_sulfur, records), 2):
                if ri.resnum == rj.resnum and ri.segment == rj.segment:
                    # We might get here if we have two alt-locs of the same sulfur atom
                    continue
                delta = ri.coords - rj.coords
                dist_sq = np.sum(np.multiply(delta, delta, delta))
                if dist_sq > DISU_THRESH_MIN and dist_sq < DISU_THRESH_MAX:
                    disulfides.append((ri.segment.strip(), ri.resnum,
                                       rj.segment.strip(), rj.resnum))

        for link in disulfides:
            commands.append("patch DISU {0}:{1} {2}:{3}".format(*link))

        commands += coord_commands

        commands.append("guesscoord")
        commands.append("writepsf charmm {0}.psf".format(out_prefix))
        if xplor_psf:
            commands.append("writepsf x-plor {0}_xplor.psf".format(out_prefix))
        commands.append("writepdb {0}.pdb".format(out_prefix))

        logger.debug("\n".join(commands))

        stdout, stderr = p.communicate(("\n".join(commands)).encode())

        if minimize and nsteps > 0:
            nmin = which(nmin, required=True)

            input_file = "{0}.pdb".format(out_prefix)
            fixed_atoms_file = workdir / "fixed.pdb"
            with open(input_file, "r") as inp, open(fixed_atoms_file, "w") as out:
                for line in inp:
                    if line.startswith("ATOM") and line[55:60] != ' 0.00':
                        out.write(line)

            cmd = [nmin, "{0}.psf".format(out_prefix), prm, rtf,
                   "{0}.pdb".format(out_prefix), workdir / "fixed.pdb",
                   str(nsteps)]
            with open("{0}_nmin.pdb".format(out_prefix), "w") as min_out:
                subprocess.check_call(cmd, stdout=min_out)
    except:
        logger.exception(
            "Unexpected error: Working files location in {}".format(workdir))
        raise
    else:
        if delete_tmp:
            import shutil
            shutil.rmtree(workdir)
        else:
            print("Working files location in {}".format(workdir))
