.. sblu documentation master file, created by
   sphinx-quickstart on Tue Dec  1 18:22:10 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sblu's documentation!
================================

sblu is the Structural Bioinformatics Laboratory Utilities library. Its an
assortment of loosely coupled Python functions that help you work with the
different types of data used in Structural Bioinformatics Lab.

The main goals of this library are:

- Correctness. All functions should be well tested and be free of bugs.
- Maintainability. Leverage existing high quality libraries as much as possible
  and have clean, well documented code.
- Composability. Functions should be easily reuseable and composable.

Installation and Upgrade:

Just one ``pip install sblu`` away! To upgrade, try ``pip install -U sblu``.

Contents
--------

.. toctree::
   :maxdepth: 2

   quickstart
   api
   command_line

Miscellaneous Pages
-------------------

.. toctree::
   :maxdepth: 2

   changelog

* :ref:`search`
