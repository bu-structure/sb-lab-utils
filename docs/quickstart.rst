Quickstart
==========

.. currentmodule:: sblu

Install from PyPI::

  pip install sblu

After installation, you can see all the  sblu subcommands::

  sblu
