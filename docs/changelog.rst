Changelog
=========

v0.4.4
------

- Fix pwrmsd bug.
- Add subcommand ``docking cluster`` for clustering pairwise RMSD files.

v0.4.3
------

- Fix inconsistent --interface option for pwrmsd.
- Fix segment ordering for ``pdb prep``.

v0.4.0
------

- Add ``sblu prep`` subcommand.
